<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Image;

class ImagesController extends Controller {
    function index() {
        return json_encode(Image::orderBy('created_at', 'desc')->get());
    }

    function store(Request $request) {
        $file = $request->file('file');
        $image = str_replace('data:image/png;base64,', '', base64_encode(file_get_contents($file->path())));

        $res = file_get_contents('https://test.rxflodev.com', false, stream_context_create([
            'http' => [
                'method' => 'POST',
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'content' => http_build_query([
                    'imageData' => $image
                ])
            ]
        ]));

        $res = json_decode($res);

        $rec = new Image();
        $rec->created_at = now();
        $rec->updated_at = now();
        $rec->url = $res->url;
        $rec->save();

        echo json_encode($res);
    }   
}
